#include "moteur.h"

Moteur::Moteur(int numero_serie) {
    this->numero_serie = numero_serie;
}

int Moteur::getNbPistons() {
    return pistons.size();
}

int Moteur::getNumeroSerie(){
    return this->numero_serie;
}

Moteur::~Moteur() {
    for (int p=0 ; p<pistons.size() ; p++) {
	
	// Obtient le pointeur vers le dernier objet.
        Piston* un_piston = pistons.back();
	
	// Enlève le dernier objet du vecteur.
	pistons.pop_back();

	// Efface l'objet de la mémoire.
        delete un_piston;
    }
}

